#!/usr/bin/env bash
# vim: autoindent tabstop=2 shiftwidth=2 expandtab softtabstop=2 fileencoding=utf-8
#

[[ $DEBUG ]] && set -x
if [[ -z "$FORMSERVER_URL" ]]; then
  echo "Missing required variable FORMSERVER_URL"
  exit 9
fi

wait-for-it "$FORMSERVER_URL"
http --check-status "$FORMSERVER_URL"/form/5d5a66b7669977001b5b617b || (http post "$FORMSERVER_URL"/form < /data/address.json)
http --check-status "$FORMSERVER_URL"/form/5d5a45a8669977001b5b6179 || (http post "$FORMSERVER_URL"/form < /data/birth-info.json)
http --check-status "$FORMSERVER_URL"/form/5d7aa1b318fecd734051ae80 || (http post "$FORMSERVER_URL"/form < /data/fiscal-code.json)
http --check-status "$FORMSERVER_URL"/form/5d4d26ff9410f50010f30068 || (http post "$FORMSERVER_URL"/form < /data/full-name.json)
http --check-status "$FORMSERVER_URL"/form/5d5a41a4669977001b5b6177 || (http post "$FORMSERVER_URL"/form < /data/gender.json)
http --check-status "$FORMSERVER_URL"/form/6270dc5056f4af00183bc1ae || (http post "$FORMSERVER_URL"/form < /data/residency.json)
http --check-status "$FORMSERVER_URL"/form/62306f897daf240019e9eb59 || (http post "$FORMSERVER_URL"/form < /data/iban.json)
http --check-status "$FORMSERVER_URL"/form/627cbd1e56f4af00183f084f || (http post "$FORMSERVER_URL"/form < /data/minor-personal-data.json)
http --check-status "$FORMSERVER_URL"/form/5d7aa15b18fecd734051ae7f || (http post "$FORMSERVER_URL"/form < /data/personal-data.json)
http --check-status "$FORMSERVER_URL"/form/5e5e26ede170600020175850 || (http post "$FORMSERVER_URL"/form < /data/personal-data-light.json)
http --check-status "$FORMSERVER_URL"/form/605dd397a406c00020e9eef6 || (http post "$FORMSERVER_URL"/form < /data/personal-data-minimal.json)
http --check-status "$FORMSERVER_URL"/form/624e8bb556f4af00183434f3 || (http post "$FORMSERVER_URL"/form < /data/personal-data-contacts.json)
http --check-status "$FORMSERVER_URL"/form/64830614076888004cc528bc || (http post "$FORMSERVER_URL"/form < /data/privacy.json)
http --check-status "$FORMSERVER_URL"/form/64833e23076888004cc7544e || (http post "$FORMSERVER_URL"/form < /data/beneficiary.json)
http --check-status "$FORMSERVER_URL"/form/64818ac034465b004c539fb3 || (http post "$FORMSERVER_URL"/form < /data/mandate.json)
http --check-status "$FORMSERVER_URL"/form/6481904534465b004c53a856 || (http post "$FORMSERVER_URL"/form < /data/legal-entity.json)
http --check-status "$FORMSERVER_URL"/form/6270fa3c56f4af00183bd801 || (http post "$FORMSERVER_URL"/form < /data/real-estate.json)
http --check-status "$FORMSERVER_URL"/form/6270f97756f4af00183bd7d6 || (http post "$FORMSERVER_URL"/form < /data/vehicles.json)
http --check-status "$FORMSERVER_URL"/form/62306a607daf240019e9e952 || (http post "$FORMSERVER_URL"/form < /data/isee.json)
http --check-status "$FORMSERVER_URL"/form/648b02ea076888004cd213f3 || (http post "$FORMSERVER_URL"/form < /data/spouse.json)
http --check-status "$FORMSERVER_URL"/form/6482df7934465b004c54a4f3 || (http post "$FORMSERVER_URL"/form < /data/oc-iban.json)
http --check-status "$FORMSERVER_URL"/form/66e95788e830010045715850 || (http post "$FORMSERVER_URL"/form < /data/inefficiencies-backoffice.json)