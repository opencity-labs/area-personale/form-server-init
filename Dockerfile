FROM lorello/alpine-bash:1.2.0

WORKDIR /

COPY data ./data
COPY init.sh ./
RUN chmod 755 /init.sh

CMD ["/init.sh"]